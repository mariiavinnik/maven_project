package hw17;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MatrixTest {
    Matrix matrix;

    @BeforeEach
    void setUp() {
        int[][] matrix = {{1, 2, 3}, {4, -10, 5}, {4, 8, 10}};
        this.matrix = new Matrix(matrix);
    }

    @Test
    void matrixNotNull() {
        Assertions.assertNotNull(matrix.getMatrix());
    }

    @Test
    void isSomeOfElementsNull() {
        int[][] matrixValue = matrix.getMatrix();

        for (int[] line : matrixValue) {
            Assertions.assertNotNull(line);
        }
    }

    @Test
    void calculateAvg() {
        double avgOfMatrixElements = matrix.calculateAvgOfMatrixElements();

        Assertions.assertEquals(3.0, avgOfMatrixElements, 0.001);
    }

    @Test
    void calculateAvgForEmptyArray() {
        int[][] matrixWithEmptyArrays = {{}, {}, {}, {}};
        Matrix instance = new Matrix(matrixWithEmptyArrays);

        double avgForMatrixWithEmptyArrays = instance.calculateAvgOfMatrixElements();

        Assertions.assertEquals(0.0, avgForMatrixWithEmptyArrays, 0.001);
    }

    @Test
    void notEmptyMatrix() {
        int[][] matrixValue = matrix.getMatrix();

        Assertions.assertNotEquals(0, matrixValue.length);
    }

    @Test
    void notEmptyMatrixArrays() {
        int[][] matrixValue = matrix.getMatrix();

        for (int[] line : matrixValue) {
            Assertions.assertNotEquals(0, line.length);
        }
    }

    @Test
    void squareMatrix() {
        boolean result = matrix.isMatrixSquare();

        Assertions.assertTrue(result);
    }

    @Test
    void notSquareMatrix() {
        int[][] matrixValue = {{1, 2}, {3, 4, 4}};
        Matrix instance = new Matrix(matrixValue);

        boolean result = instance.isMatrixSquare();

        Assertions.assertFalse(result);
    }
}