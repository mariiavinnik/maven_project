package hw17;

public class Matrix {
    private int[][] matrix;

    public Matrix(int[][] matrix) {
        if (matrix == null) {
            printNullError();
        } else {
            boolean isSomeElementsNull = false;
            for (int[] line : matrix) {
                if (line == null) {
                    isSomeElementsNull = true;
                }
            }
            if (isSomeElementsNull) {
                printNullError();
            } else {
                this.matrix = matrix;
            }
        }
    }

    public int[][] getMatrix() {
        return matrix;
    }

    private static void printNullError() {
        System.out.println("Null Error");
    }

    public double calculateAvgOfMatrixElements() {
        double avg = 0;
        int numOfEls = 0;
        for (int[] line : matrix) {
            for (int num : line) {
                avg += num;
                numOfEls++;
            }
        }
        if (numOfEls == 0) {
            return 0.0;
        } else {
            return avg / numOfEls;
        }
    }

    public boolean isMatrixSquare() {
        if (matrix.length == 0) {
            return false;
        } else {
            boolean isSquare = true;
            for (int[] line : matrix) {
                if (line.length != matrix.length) {
                    isSquare = false;
                }
            }
            return isSquare;
        }
    }

    public static void main(String[] args) {
        Matrix matrix;
        int[][] m = {{1, 2, 3}, {4, 8, 5}, {4, 8, 10}};
        matrix = new Matrix(m);
        int[][] matrixValue = matrix.getMatrix();

        for (int[] line : matrixValue) {
            for (int num : line) {
                System.out.println(num);
            }
        }
    }
}
