package mariia.vynnyk;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws WrongLoginException, WrongPasswordException {
        Scanner scanner = new Scanner(System.in);

        String login = null;
        String password = null;
        String confirmPassword = null;

        int tries = 3;
        int i = 0;
        User user = null;

        do {
            try {
                System.out.println("Enter login:");
                login = scanner.next();

                System.out.println("Enter password:");
                password = scanner.next();

                System.out.println("Confirm your password:");
                confirmPassword = scanner.next();

                user = new User(login, password, confirmPassword);
                System.out.println("User was created");
                break;
            } catch (WrongLoginException | WrongPasswordException e) {
                i++;
                if (i >= tries) {
                    System.out.println("User can't be created");
                    break;
                }
                System.out.println(e.getMessage());
            } finally {
                System.out.println("Thank you for using our service");
            }
        } while (user == null && i <= tries);
    }
}
