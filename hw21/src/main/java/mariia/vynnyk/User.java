package mariia.vynnyk;

import java.util.Scanner;

public class User {
    private final String login;
    private final String password;

    public User(String login, String password, String confirmPassword) throws WrongLoginException, WrongPasswordException {
        if (login != null && login.length() <= 20 && login.matches("[a-zA-Z]+") && login != "") {
            this.login = login;
        } else {
            throw new WrongLoginException();
        }

        if (password != null && password.matches("[a-zA-Z0-9]*") && isContainNumber(password) && isContainLetter(password) && password.length() <= 25 && password.length() >= 6 && password.equals(confirmPassword)) {
            this.password = password;
        } else {
            throw new WrongPasswordException();
        }
    }

    private boolean isContainNumber(String str) {
        char[] chars = str.toCharArray();

        for (char c : chars) {
            if (Character.isDigit(c)) {
                return true;
            }
        }

        return false;
    }

    private boolean isContainLetter(String str) {
        char[] chars = str.toCharArray();

        for (char c : chars) {
            if (Character.isLetter(c)) {
                return true;
            }
        }

        return false;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }
}