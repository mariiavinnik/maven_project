package mariia.vynnyk;

public class WrongLoginException extends Exception {
    public WrongLoginException() {
        super("Login has to contain not more than 20 characters and letters of the English alphabet only.");
    }
}
