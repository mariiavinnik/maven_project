package mariia.vynnyk;

public class WrongPasswordException extends Exception {
    public WrongPasswordException() {
        super("Password has to contain Latin letters and numbers only. " +
                "Must contain at least 1 letter and 1 number. \nThe length of the password " +
                "must be from 6 to 25 characters. The password must match the confirmation " +
                "password."
        );
    }
}
