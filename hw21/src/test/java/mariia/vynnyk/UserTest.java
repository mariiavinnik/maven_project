package mariia.vynnyk;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UserTest {
    final String expectedExceptionLoginMessage = "Login has to contain not more than 20 characters and letters of the English alphabet only.";
    final String expectedExceptionPasswordMessage = "Password has to contain Latin letters and numbers only. " +
            "Must contain at least 1 letter and 1 number. \nThe length of the password " +
            "must be from 6 to 25 characters. The password must match the confirmation " +
            "password.";

    @Test
    void correctPassword() throws WrongLoginException, WrongPasswordException {
        User user = new User("rawa", "rrrrrrrrr5", "rrrrrrrrr5");

        String password = user.getPassword();

        Assertions.assertEquals(password, "rrrrrrrrr5");
    }

    @Test
    void correctLogin() throws WrongLoginException, WrongPasswordException {
        User user = new User("rawa", "rrrrrrrrr5", "rrrrrrrrr5");

        String login = user.getLogin();

        Assertions.assertEquals(login, "rawa");
    }

    @Test
    void loginWithNumber() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongLoginException.class, () -> {
            new User("rawa5", "rrrrrrrrr5", "rrrrrrrrr5");
        }, expectedExceptionLoginMessage);
    }

    @Test
    void loginWithNotLatinLetters() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongLoginException.class, () -> {
            new User("rawaроаі", "rrrrrrrrr5", "rrrrrrrrr5");
        }, expectedExceptionLoginMessage);
    }

    @Test
    void loginWithMoreThan20Letters() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongLoginException.class, () -> {
            new User("rawarrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr", "rrrrrrrrr5", "rrrrrrrrr5");
        }, expectedExceptionLoginMessage);
    }

    @Test
    void loginEmptyString() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongLoginException.class, () -> {
            new User("", "rrrrrrrrr5", "rrrrrrrrr5");
        }, expectedExceptionLoginMessage);
    }

    @Test
    void loginNull() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongLoginException.class, () -> {
            new User(null, "rrrrrrrrr5", "rrrrrrrrr5");
        }, expectedExceptionLoginMessage);
    }

    @Test
    void passwordNull() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("rawarrryyy", null, "rrrrrrrrr");
        }, expectedExceptionPasswordMessage);
    }

    @Test
    void passwordWithoutNumber() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("rawarrryyy", "rrrrrrrrr", "rrrrrrrrr");
        }, expectedExceptionPasswordMessage);
    }

    @Test
    void passwordWithoutLetter() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("rawarrryyy", "55555555", "55555555");
        }, expectedExceptionPasswordMessage);
    }

    @Test
    void wrongLengthOfPassword() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("rawarrryyy", "l5", "l5");
        }, expectedExceptionPasswordMessage);
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("rawarrryyy", "lllllll666666lllllll666666lllllll666666lllllll666666",
                    "lllllll666666lllllll666666lllllll666666lllllll666666"
            );
        }, expectedExceptionPasswordMessage);
    }

    @Test
    void wrongConfirmedPassword() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("rawarrryyy", "rrrrrrrrr5", "rrrrrrrrr56");
        }, expectedExceptionPasswordMessage);
    }

    @Test
    void passwordWithNotLatinLetters() throws WrongLoginException, WrongPasswordException {
        Assertions.assertThrowsExactly(WrongPasswordException.class, () -> {
            new User("dflkljgkesd", "rrrrпппrrrrr5", "rrrrпппrrrrr5");
        }, expectedExceptionPasswordMessage);
    }
}
