package vynnyk;

import java.util.Objects;

public class Point implements Cloneable {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(Point point) {
        if (point == null) {
            printNullError();
        } else {
            this.x = point.x;
            this.y = point.y;
        }
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    private static void printNullError() {
        System.out.println("Objects can not be null !!!");
    }

    public static double calculateDistanceBtwTwoPoints(Point firstPoint, Point secondPoint) {
        if (firstPoint != null && secondPoint != null) {
            double distance = Math.sqrt(Math.pow((secondPoint.x - firstPoint.x), 2) + Math.pow((secondPoint.y - firstPoint.y), 2));
            return distance;
        } else {
            printNullError();
            return 0.0;
        }
    }

    public double calculateDistanceToOtherPoint(Point secondPoint) {
        return calculateDistanceBtwTwoPoints(this, secondPoint);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Point that = (Point) obj;
        return (this.x == that.x && this.y == that.y);
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Point {" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public Point clone() throws CloneNotSupportedException {
        return (Point) super.clone();
    }
}
