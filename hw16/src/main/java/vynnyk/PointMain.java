package vynnyk;

public class PointMain {
    public static void main(String[] args) throws CloneNotSupportedException {
        Point[] points = new Point[4];
        points[0] = new Point(2, 2);
        points[1] = new Point(points[0]);
        points[2] = new Point(-3, 5);
        points[3] = points[2].clone();

        System.out.println(points[0].toString());
        System.out.println(points[1].toString());
        System.out.println(points[2].toString());
        System.out.println(points[3].toString());

        System.out.println("if point1 equals point2 => " + points[0].equals(points[1]));
        System.out.println("if point3 equals point1 => " + points[2].equals(points[0]));
        System.out.println("if point3 equals point4 => " + points[2].equals(points[3]));

        System.out.println("Distance between point1 and point2 => " + points[0].calculateDistanceToOtherPoint(points[2]));
        System.out.println("Distance between point2 and point1 => " + Point.calculateDistanceBtwTwoPoints(points[1], points[0]));
        System.out.println("Distance between point3 and point1 => " + Point.calculateDistanceBtwTwoPoints(points[2], points[0]));
    }
}
