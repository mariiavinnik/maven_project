package vynnyk;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class PointTest {
    @Test
    void equals() {
        Point point1 = new Point(-2, 2);
        Point pointCopy = new Point(point1);
        Integer[] notPoint = {-2, 2};
        Point point2 = new Point(-2, 2);
        int x1 = point1.getX();
        int x2 = pointCopy.getX();
        int y1 = point1.getY();
        int y2 = pointCopy.getY();

        boolean samePointsResult = point1.equals(pointCopy);
        boolean samePointsSуmmetricResult = pointCopy.equals(point1);
        boolean nullPointResult = pointCopy.equals(null);
        boolean comparisonWithNotPointResult = pointCopy.equals(notPoint);
        boolean comparisonPointToItselfResult = point1.equals(point1);
        boolean sameCoordinatesPointsResult = point2.equals(pointCopy);
        boolean sameCoordinatesOtherPointsResult = point2.equals(point1);


        assertEquals(x1, x2);
        assertEquals(y1, y2);
        assertTrue(samePointsResult);
        assertEquals(samePointsSуmmetricResult, samePointsResult);
        assertEquals(sameCoordinatesPointsResult, samePointsResult);
        assertEquals(sameCoordinatesPointsResult, sameCoordinatesOtherPointsResult);
        assertFalse(nullPointResult);
        assertFalse(comparisonWithNotPointResult);
        assertTrue(comparisonPointToItselfResult);
    }

    @Test
    void testHashCode() {
        Point point1 = new Point(-2, 2);
        Point point2 = new Point(-2, 2);

        int hashCodeOfPoint1 = point1.hashCode();
        int hashCodeOfPoint2 = point1.hashCode();

        assertNotSame(point1, point2);
        assertEquals(hashCodeOfPoint1, hashCodeOfPoint2, 901);
        assertEquals(point1, point2);
    }

    @Test
    void calculateDistanceBetweenSamePoints() {
        Point point1 = new Point(-2, 2);
        Point point2 = new Point(-2, 2);

        double result = Point.calculateDistanceBtwTwoPoints(point1, point2);

        assertEquals(0.0, result);
    }

    @Test
    void calculateDistanceToNull() {
        Point point = new Point(-2, 2);

        double result = Point.calculateDistanceBtwTwoPoints(point, null);

        assertEquals(0.0, result);
    }

    @Test
    void calculateDistanceBetweenPoints() {
        Point point1 = new Point(-3, 4);
        Point point2 = new Point(0, 0);

        double result = Point.calculateDistanceBtwTwoPoints(point1, point2);

        assertEquals(5.0, result, 0.0001);
    }

    @Test
    void calculateDistanceToOtherPoint() {
        Point point1 = new Point(-3, 4);
        Point point2 = new Point(0, 0);

        double result = point1.calculateDistanceToOtherPoint(point2);

        Assertions.assertEquals(5.0, result, 0.0001);
    }

    @Test
    void cloneConstructor() {
        Point point1 = new Point(-3, 4);
        Point sameToPoint1 = point1;
        Point point2 = new Point(point1);

        boolean point2EqualToPoint1Result = point2.equals(point1);
        boolean point1HaveNoChangesResult = sameToPoint1 == point1;

        Assertions.assertTrue(point2EqualToPoint1Result);
        Assertions.assertTrue(point1HaveNoChangesResult);
    }
}